import { Component, Input, Output, OnInit, Injectable,
    OnChanges, SimpleChanges, EventEmitter}
    from '@angular/core';
import {Product} from "./product";



@Component({
    selector : 'ngshop-page-numbers',
    template : `<div class="pagination-centered left" *ngIf="pageNumbers.length > 0">
                  <ul class="pagination">
                    <li class="arrow" (class.unavailable)="currentPage == 0"><a href="javascript:void(0)"
                     (click)="onPreviousClick()">&laquo;</a></li>
                    <li *ngFor="let page of pageNumbers"
                        (class.current)="page == (currentPage + 1)">
                        <a href="javascript:void(0)" (click)="onPageClick(page)">{{page}}</a></li>
                    <li class="arrow" (class.unavailable)="currentPage==(Math.ceil(total/pagesize)-1)">
                    <a href="javascript:void(0)" (click)="onNextClick()">&raquo;</a></li>
                  </ul>
                 </div>`
})
export class PageNumbersComponent implements OnInit, OnChanges{

    ngOnChanges(changes:SimpleChanges):any {
        this.createPageNumbers();
    }

    ngOnInit():any {
        this.createPageNumbers();
    }

    onPageClick(page: number){
        this.currentPage = page - 1;
    }

    onNextClick(){
        if(this.currentPage != (Math.ceil(this.total/this.pagesize) - 1) ){
            this.currentPage ++;
        }
    }

    onPreviousClick(){
        if(this.currentPage > 0){
            this.currentPage --;

        }
    }

    set currentPage(val:number){
        this._currentPage = val;
        this.pageNumberChanged.emit(this._currentPage + 1);
    }

    get currentPage(){
        return this._currentPage;
    }

    @Input()
    pagesize : number;

    @Input()
    total : number;

    @Output()
    pageNumberChanged = new EventEmitter();

    pageNumbers : number[] = [];

    private _currentPage : number = 0;

    private createPageNumbers(){
        this.pageNumbers = [];
        const maxPage = Math.ceil(this.total/this.pagesize);
        for(let i = 0; i<maxPage; ++i){
            this.pageNumbers.push(i+1);
        }
    }
}

