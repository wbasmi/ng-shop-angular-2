import { bootstrap } from "@angular/platform-browser-dynamic";
import { AppComponent } from "./app.component";
import { appRoutesProviders } from './app.routes';
bootstrap(AppComponent,
    appRoutesProviders);
