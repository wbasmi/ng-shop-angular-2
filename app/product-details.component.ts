import { Component, OnInit, OnDestroy } from '@angular/core';
import {Product} from "./product";
import { ActivatedRoute } from '@angular/router';
import { ProductService } from './product.service';
import {CartService} from "./cart.service";
import {AddToCartButtonComponent} from "./add-to-cart-button.component";

@Component({
    selector : 'product-details',
    directives : [ AddToCartButtonComponent ],
    template : `<div class="row" style="padding-top: 10px;" *ngIf="product">
                    <section class="small-12 columns">
                        <div class="media panel panel-info">
                            <h2 class="media-heading">{{ product.name}}</h2>
                            <figure>
                            <img src="http://placehold.it/500x300" class="image media-object left"
                            style="margin-right : 15px;"/>
                            <figcaption class="row">
                                <ngshop-add-to-cart-button [product]="product"
                                [Class]="'button warning radius small-12 medium-8 medium-offset-2
                                left columns'"></ngshop-add-to-cart-button>
                            </figcaption>
                            </figure>
                            <p class="media-body" style="padding : 5px 15px;">
                            {{ product.description}}
                            </p>
                            <footer class="media-bottom">{{product.price }} DH</footer>
                            <div class="clearfix"></div>
                        </div>
                    </section>
                </div>`
})
export class ProductDetailsComponent implements OnInit, OnDestroy{

    sub ;

    ngOnInit():any {

        this.sub = this.currentRoute.params.subscribe(params => {
            this.productService.getProduct(params['id'])
                .then(product => this.product = product);
        });
    }

    ngOnDestroy():any {
        this.sub.unsubscribe();
    }

    product : Product;

    public constructor(private currentRoute : ActivatedRoute,
                        private productService : ProductService,
                        private cartService : CartService){
    }

    addToCart(product : Product){
        this.cartService.addToCart(product, 1);
    }
}