import { Component, OnInit } from '@angular/core';
import {CartService} from "./cart.service";
import {CartItem} from "./cart-item";
import {Product} from "./product";

@Component({
    selector : 'ngshop-cart-display',
    template : `<section class="row" style="padding-top : 20px;">
                    <table class="small-10 small-offset-1 medium-8 medium-offset-2 columns
                    table table-bordered table-condensed table-hover">
                        <tr><th>Name</th><th>Unit price</th><th>Quantity</th><th>Total price</th>
                        <th>total price (Tax included)</th></tr>
                        <tr *ngFor="let cartItem of cartItems">
                            <td>{{cartItem.name}}</td>
                            <td>{{cartItem.unitPrice}}</td>
                            <td>{{cartItem.qte}}</td>
                            <td>{{cartItem.priceWithoutTax}}</td>
                            <td>{{cartItem.priceWithTax }} </td>
                        </tr>
                        <tr><td></td><td></td><td></td><td>{{cartService.totalWithoutTax()}}</td>
                        <td>{{cartService.totalWithTax()}}</td></tr>
                    </table>
                </section>`
})
export class CartDisplayComponent implements OnInit{


    tax : number;

    cartItems : CartItem [];

    ngOnInit() {
        this._prepareCart();
    }

    public constructor(private cartService : CartService){
    }

    private _prepareCart(){
        this.cartItems = Array.from(this.cartService.items.values());
        this.tax = Product.tax;
    }
}