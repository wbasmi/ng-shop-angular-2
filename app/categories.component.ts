import { Component, OnInit } from '@angular/core';
import {ProductService} from "./product.service";

@Component({
    selector : 'ngshop-categories',
    template : `<ul class="list-group vertical">
                   <li *ngFor='let category of categories'
                   class="list-group-item no-bullet">{{category}}</li>
                </ul>`
})
export class CategoriesComponent implements OnInit{
    categories : string[] = [];

    constructor(private productService : ProductService){
    }

    ngOnInit() {
        this.getCategories();
    }

    private getCategories(){
        this.productService
            .getCategories()
            .then(categories => this.categories = categories);
    }
}
