export class Product{
    id : number;
    name : string;
    price : number;
    category : string;
    description : string;
    static tax : number = 0.15;
}
