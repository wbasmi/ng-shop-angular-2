import { Product } from "./product";


export class CartItem{

    constructor(private _product : Product, private _qte : number = 0){
    }

    get priceWithTax(){
        return this._product.price * this._qte * (1 + Product.tax);
    }

    get priceWithoutTax(){
        return this._product.price * this._qte;
    }

    get qte(){
        return this._qte;
    }

    set qte(qte : number){
        this._qte = qte;
    }

    get unitPrice(){
        return this._product.price;
    }

    get name(){
        return this._product.name;
    }

    get id(){
        return this._product.id;
    }
}
