import { Injectable } from '@angular/core';
import { PRODUCTS } from './products-mock';
import {Product} from "./product";


@Injectable()
export class ProductService{

    products : Product[] = PRODUCTS;

    getCategories(){
        return new Promise<string[]>( resolve => setTimeout(() => {
            let categories : Map<string,boolean> = new Map<string,boolean>();
            let result : string[] = [];
            for(let product of this.products){
                if(! categories.has(product.category) ){
                    categories.set(product.category, true);
                    result.push(product.category);
                }
            }
            resolve(result);
        }, 1000));
    }

    getProduct(id : number ){
        return new Promise<Product>(resolve => setTimeout( () => {
            for(let product of this.products){
                if(product.id == id){
                    resolve(product);
                    break;
                }
            }
        }, 1000));
    }


    getProductsOfCategory(category : string = null){
        return new Promise<Product[]>( resolve => setTimeout(() => {
            if(!category) resolve(this.products);
            let result : Product[] = [];
            for(let product of this.products){
                if(product.category === category){
                    result.push(product);
                }
            }
            resolve(result);
        }, 1000));
    }

}