import { PipeTransform, Pipe } from '@angular/core';
import {Product} from "./product";

@Pipe({
    name : 'filter'
})
export class FilterPipe implements PipeTransform{

    transform(data: Product[], options : any):any {
        if(data.length == 0) return [];
        let propertyValues : Map<number,boolean> = new Map<number,boolean>();
        let result : Product[] = [];
        for(let product of data){
            if(!propertyValues.has(product['id']))
            {
                let ok : boolean = true;
                for(let property in options)
                {
                    if( options[property]!=null && product[property] !== options[property] ){
                        ok = false;
                        break;
                    }
                }
                if(ok)
                {
                    propertyValues.set(product['id'], true);
                    result.push(product);
                }
            }
        }
        return result;
    }

}
