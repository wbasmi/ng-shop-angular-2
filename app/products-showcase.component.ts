import { Component, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import { ActivatedRoute, ROUTER_DIRECTIVES } from '@angular/router';
import { ProductService } from "./product.service";
import { Product } from "./product";
import { ExtractPipe } from './extract.pipe';
import {FilterPipe} from "./filter.pipe";
import {ProductDisplayComponent} from "./product-display.component";
import {OrderByPipe} from "./order-by.pipe";
import {PageNumbersComponent} from "./page-numbers.component";
import {PaginatePipe} from "./paginate.pipe";

@Component({
    selector : 'ngshop-products-vitrine',
    pipes : [ ExtractPipe , FilterPipe, OrderByPipe, PaginatePipe],
    directives : [ProductDisplayComponent , PageNumbersComponent, ROUTER_DIRECTIVES],
    template : `<div class="row" style="padding-top: 10px;">
                    <div class="small-12 medium-2 columns">
                        <ul class="side-nav" role="navigation" title="Link List">
                            <li role="menuitem" (click)="onSelectCategory(null)"
                            [class.active]="category == selectedCategory" *ngIf="products.length">
                                <a href="javascript:void(0)" target="_self">All</a>
                            </li>
                           <li role="menuitem" *ngFor="let category of products | extract : 'category'"
                                [class.active]="category == selectedCategory"
                                (click)="onSelectCategory(category)">
                                <a href="javascript:void(0)" target="_self">{{category}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="small-12 medium-10 columns">
                        <div class="row">
                            <div class="small-12 columns" style="padding-top : 5px;padding-bottom : 5px;"
                                *ngIf="products.length">
                                <label class="right">
                                    <b style="font-size : 15px;">order by</b>
                                    <div style="display : inline-block;padding-left : 10px;" (click)="onOrderBy('name')">
                                        name
                                        <span class="fa fa-caret-down" *ngIf="order.name && order.name=='desc'"></span>
                                        <span class="fa fa-caret-up" *ngIf="order.name && order.name=='asc'"></span>
                                    </div>
                                    <div style="display : inline-block;padding-left : 10px;" (click)="onOrderBy('price')">
                                        price
                                        <span class="fa fa-caret-down" *ngIf="order.price && order.price=='desc'"></span>
                                        <span class="fa fa-caret-up" *ngIf="order.price && order.price=='asc'"></span>
                                    </div>
                                </label>
                            </div>
                            <div *ngFor='let product of products | filter : { category : selectedCategory } |
                                    paginate : { page : selectedPage, size : pageSize } | orderBy : order '
                                class="small-12 medium-4 columns left">
                                <ngshop-product-display [product]="product"></ngshop-product-display>
                            </div>
                            <div class="small-12 columns" *ngIf="products.length">
                                <ngshop-page-numbers [pagesize]="pageSize"
                                [total]="(products | filter : { category : selectedCategory }).length"
                                (pageNumberChanged)="setSelectedPage($event)">
                                </ngshop-page-numbers>
                                <ul class="button-group right" style="padding : 0 10px;">
                                  <li><a href="javascript:void(0)" target='_self'
                                  class="button secondary small"
                                     (click)="onPageSizeSelect(6)">6</a></li>
                                  <li><a href="javascript:void(0)" target='_self' class="button secondary small"
                                       (click)="onPageSizeSelect(12)">12</a></li>
                                  <li><a href="javascript:void(0)" target='_self' class="button secondary small"
                                    (click)="onPageSizeSelect(24)">24</a></li>
                                </ul>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                    </div>
                </div>`
})
export class ProductsShowcaseComponent implements OnInit{

    order = {};

    pageSize : number = 6;

    selectedPage : number = 1;

    products : Product[] = [];

    selectedCategory : string = null;

    constructor(private productService : ProductService,
                private route : ActivatedRoute){
    }

    onSelectCategory(category : string){
        this.selectedCategory = category;
    }

    setSelectedPage(page){
        this.selectedPage = page;
    }


    onPageSizeSelect(pageSize : number = 5 ){
        this.pageSize = pageSize;
    }

    onOrderBy(property : string){
        for(let p in this.order)
            if(property!=p) delete this.order[p];

        if(!this.order[property])
            this.order[property] = 'asc';
        else {
            if(this.order[property] == 'asc')
                this.order[property] = 'desc';
            else this.order[property] = 'asc';
        }
    }


    ngOnInit() {
        this.route.params.subscribe(params => {
            let category = params['category'];
            this.getProducts(category);
        });
    }



    private getProducts(category : string = null){
        this.productService
            .getProductsOfCategory(category)
            .then(products => this.products = products);
    }
}
