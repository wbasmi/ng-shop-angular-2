import { Component, Input } from '@angular/core';
import {Product} from "./product";
import { Router } from '@angular/router';
import {AddToCartButtonComponent} from "./add-to-cart-button.component";


@Component({
    selector : 'ngshop-product-display',
    directives : [ AddToCartButtonComponent ],
    template : `<article class="panel" *ngIf="product">
                <h4 class="panel-heading">{{product.name}}</h4>
                <section class="panel-body" style="max-height : 150px;overflow: hidden">
                    <p>{{ product.description.slice(0, 127) }}</p>
                </section>
                <section class="panel-footer">
                    <hr />
                    <p class="price text-center">{{ product.price }} DH</p>
                    <div class="row">
                    <ngshop-add-to-cart-button [product]="product"
                                [Class]="'button warning radius small-12 medium-8 medium-offset-2
                                left columns'"></ngshop-add-to-cart-button>
                    </div>
                </section>
                </article>`
})
export class ProductDisplayComponent {
    @Input()
    product : Product;

    constructor(private router : Router){
    }

    onDetailsClick(product : Product){
        const link = ['/detail', product.id]
        this.router.navigate(link);
    }
}