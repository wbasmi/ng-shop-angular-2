import { Injectable } from '@angular/core';
import {Product} from "./product";
import {CartItem} from "./cart-item";


@Injectable()
export class CartService {

    private _items : Map<number , CartItem> = new Map<number, CartItem>();

    constructor(){
    }
    addToCart(product: Product, qte : number = 1){
        if(this._items.has(product.id)){
            this._items.get(product.id).qte +=qte ;
        }else{
            this._items.set(product.id, new CartItem(product, qte));
        }
    }

    get items(){
        return this._items;
    }

    totalWithTax(){
        let total = 0;
        this._items.forEach((value, key) =>
            total += value.unitPrice * value.qte
        );

        return Math.round( total * (Product.tax + 1) * 100) / 100;
    }

    totalWithoutTax(){
        let total = 0;
        this._items.forEach((value, key) =>
            total += value.unitPrice * value.qte
        );
        return Math.round( total  * 100) / 100;
    }
}
