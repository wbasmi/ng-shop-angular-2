import { Component, OnInit} from '@angular/core';
import { ProductService} from './product.service';
import {Product} from "./product";
import { ProductDisplayComponent } from './product-display.component';


@Component({
    selector : 'ngshop-home',
    directives : [ ProductDisplayComponent ],
    template : `<h2>Welcome to Ng Shop !</h2>
                <section class="row">
                    <h3 class="small-12 column">Top Products</h3>
                    <div class="small-12 medium-3 column" *ngFor='let product of topProducts'>
                        <ngshop-product-display [product]="product"></ngshop-product-display>
                    </div>
                </section>`
})
export class HomeComponent implements OnInit{
    ngOnInit() {
        this.getTopProducts();
    }

    topProducts : Product[] = [];

    constructor(private productService : ProductService ){
    }

    private getTopProducts() {
        this.productService.getProductsOfCategory()
            .then(products => this.topProducts = products.slice(0,4));
    }
}