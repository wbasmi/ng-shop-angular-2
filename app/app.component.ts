import { Component } from  '@angular/core';
import { CategoriesComponent } from './categories.component';
import { ProductService } from './product.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {CartService} from "./cart.service";
import {Product} from "./product";


@Component({
    selector : 'ngshop-app',
    directives : [ ROUTER_DIRECTIVES],
    providers : [
        ProductService,
        CartService
    ],
    template : `<nav class="top-bar">
                    <ul class="title-area">
                        <li class="name">
                            <h1><a [routerLink]="['/']">{{title}}</a></h1>
                        </li>
                    </ul>
                    <section class="top-bar-section">
                        <ul class="right">
                            <li><a [routerLink]="['/home']">Home</a></li>
                            <li><a [routerLink]="['/products']">Products</a></li>
                            <li class="success">
                                <a [routerLink]="['/cart']">Cart <i class="fa fa-2x fa-shopping-cart"></i>
                                {{total()}} DH
                                </a>
                            </li>
                        </ul>
                    </section>
                </nav>
                <main>
                    <router-outlet></router-outlet>
                </main>
                `
})
export class AppComponent{
    title = "NgShop";

    constructor(private cartService : CartService){
    }

    total(){
        return this.cartService.totalWithTax();
    }
}
