import { Pipe, PipeTransform } from '@angular/core';
import {Product} from "./product";



@Pipe({
    name : 'orderBy',
    pure : false
})
export class OrderByPipe implements PipeTransform{
    compareFN;

    transform(data:any, orderProperties:any):any {

        if(data.length == 0) return data;

        this.compareFN = function(a, b){

            for(let orderProperty in orderProperties){

                const order = orderProperties[orderProperty] == 'asc'? 1:-1;

                let cmp ;
                if(typeof a[orderProperty] == "string") {

                    if ((cmp = a[orderProperty].localeCompare(b[orderProperty])))
                        return cmp * order;
                }
                else if(typeof a[orderProperty] == "number") {
                        if (a[orderProperty] > b[orderProperty])
                            return -1 * order;

                        if (a[orderProperty] < b[orderProperty])
                            return order;

                }else throw new Error('cannot compare data')
            }
            return 0;
        };

        return data.sort(this.compareFN);
    }
    static n : number = 0;
}
