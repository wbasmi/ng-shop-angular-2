import { provideRouter, RouterConfig } from '@angular/router';
import { HomeComponent } from './home.component';
import { CategoriesComponent } from './categories.component';
import {ProductsShowcaseComponent} from "./products-showcase.component";
import {ProductDetailsComponent} from "./product-details.component";
import {CartDisplayComponent} from "./cart-display.component";

const routes: RouterConfig = <RouterConfig>[
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path : 'products',
        component : ProductsShowcaseComponent
    },
    {
        path : '',
        redirectTo : '/home',
        pathMatch : 'full'
    },
    {
        path : 'detail/:id',
        component : ProductDetailsComponent
    },
    {
        path : 'cart',
        component : CartDisplayComponent
    }
];

export const appRoutesProviders = [
    provideRouter(routes)
];
