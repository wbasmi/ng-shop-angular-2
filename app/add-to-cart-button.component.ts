import { Component, Input } from '@angular/core';
import { CartService } from './cart.service';
import {Product} from "./product";

@Component({
    selector : 'ngshop-add-to-cart-button',
    template : `<button class="{{Class}}" (click)="addToCart()">
                    <span class="fa fa-shopping-cart"></span> Add</button>`
})
export class AddToCartButtonComponent{

    @Input()
    Class : string;

    @Input()
    product : Product;

    public constructor(private cartService : CartService){
    }

    addToCart(){
        this.cartService.addToCart(this.product);
    }
}
