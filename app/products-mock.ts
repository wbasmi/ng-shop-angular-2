import {Product} from "./product";


export const PRODUCTS : Product[] = [
    {
        id : 7,
        name : 'baseball',
        price : 50,
        category : 'Ball',
        description : `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
         sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
           Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
           Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
           invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.`
    },
    {
        id : 8,
        name : 'basketball',
        price : 87,
        category : 'Ball',
        description : `At vero eos et accusam
           et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus
           est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
            sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
             At vero eos et accusam et justo duo dolores et ea rebum.
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.`
    },
    {
        id : 9,
        name : 'football',
        price : 87,
        category : 'Ball',
        description : `Mus odio ab curabitur elementum rem nascetur,
         iure quibusdam hic ea! Morbi? Doloribus ante quis esse! Nunc gravida diamlorem
          quae sem eius amet posuere, eaque cursus exercitation corrupti sollicitudin sit
           potenti proident. Vivamus cubilia eleifend harum. Hic deserunt odit aute. Enim
            eos placeat curae! Facilisi, ad ex minima qui fugiat? Nibh, iusto! Molestie
             dolorem unde habitant ante officiis pellentesque architecto! Nonummy nihil`
    },
    {
        id : 10,
        name : 'blue jeans',
        price : 500,
        category : 'clothes',
        description : `Mus odio ab curabitur elementum rem nascetur,
         iure quibusdam hic ea! Morbi? Doloribus ante quis esse! Nunc gravida
          diamlorem quae sem eius amet posuere, eaque cursus exercitation corrupti
           sollicitudin sit potenti proident. Vivamus cubilia eleifend harum.
           Hic deserunt odit aute. Enim eos placeat curae! Facilisi, ad ex minima qui fugiat? Nibh,
            iusto! Molestie dolorem unde habitant ante officiis pellentesque architecto!`
    },
    {
        id : 11,
        name : 'black pants',
        price : 486,
        category : 'clothes',
        description : `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
         sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
           Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
             invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.`
    },
    {
        id : 12,
        name : 'red hat',
        price : 845,
        category : 'clothes',
        description : `Lorem ipsum dolor sit amet,
         consetetur sadipscing elitr, sed diam nonumy eirmod tempor
         invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
          At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
          kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
           Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.`
    },
    {
        id : 13,
        name : 'Asus gx487 laptop',
        price : 6556,
        category : 'laptops',
        description : `Consequuntur ante pede fusce,
         eleifend enim! Aliquid. Veritatis, arcu, est odit, wisi,
          aspernatur corrupti etiam iaculis sodales est voluptatum asperiores?
          Eveniet recusandae eiusmod vehicula optio tempus senectus fugit, perferendis.
           Parturient! Adipiscing fringilla purus nostra, blanditiis massa, facilis malesuada sequi!
           Senectus, convallis ut faucibus voluptatibus minim hac? Esse ullamcorper placeat magni
           maiores eu? Incidunt, lacus! Vivamus totam scelerisque imperdiet, impedit magna, fames
           culpa. Laborum hac error, recusandae iusto semper ullamco et pede magnis earum non
           natoque? Gravida magnis, pariatur consequuntur netus. Ea enim praesentium fuga, eget
            felis vulputate occaecat laudantium malesuada!.`
    },
    {
        id : 14,
        name :'HP t8789 laptop',
        price : 4500,
        category : 'laptops',
        description : `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
         sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
           Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
             invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
             At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
              no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
              consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
               aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.`
    },
    {
        id : 15,
        name :'Toshiba R454 laptop',
        price : 6578,
        category : 'laptops',
        description : `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
         sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
           Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
             invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
             At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
              no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
              consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
               aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.`
    }
];