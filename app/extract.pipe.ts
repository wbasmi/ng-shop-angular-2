import { PipeTransform, Pipe } from '@angular/core';
import {Product} from "./product";

@Pipe({
    name : 'extract'
})
export class ExtractPipe implements PipeTransform{

    transform(data: Product[], property : string):any {
        if(data.length == 0) return [];
        let propertyValues : Map<string,boolean> = new Map<string,boolean>();
        let result : string[] = [];
        for(let product of data){
            if(! propertyValues.has(product[property]) ){
                propertyValues.set(product[property], true);
                result.push(product[property]);
            }
        }
        return result;
    }

}