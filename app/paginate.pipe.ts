import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name : 'paginate'
})
export class PaginatePipe implements PipeTransform{

    transform(data:any, config:any):any {

        if(data.hasOwnProperty('length') && data.length == 0) return data;

        if(!config.page || !config.size)
            throw new Error('config must have the page number and the number of items per page');

        return data.slice((config.page - 1) * config.size,
            config.size + (config.page - 1) * config.size);
    }

}
